/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritanceshape;

/**
 *
 * @author a
 */
public class Rectangle extends Shape {
    protected double w;
    protected double h;


    public Rectangle(String name, double w, double h){
        super(name);
        this.w = w;
        this.h = h;
    }
    
    public double calArea(){
        return w*h;
    }

    public double getWidth() {
        return w;
    }

    public double getHeight() {
        return h;
    }
    
    @Override
    public void ShowArea(){
        if(w <= 0 || h <= 0) {
            System.out.println("Width and Height must more than 0 !!!");
        } else 
            System.out.println("Area of " + name + " : " + calArea());
    }
}
